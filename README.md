# Kubernetes-Training

## Prerequisites

- Windows 10+, OSX or Linux
- VirtualBox with Hardware virtualization enabled
- A [Docker Hub Account](https://hub.docker.com/)

## Setup 

## Approach #1
 1. Install [Minikube](https://www.docker.com/products/docker-desktop)
 2. Install Kubectl
 3. Minikbube dashboard: `minikube dashboard`


## Approach #2
 1. Install [Docker for Desktop with Kubernetes](https://www.docker.com/products/docker-desktop)


## Getting Started

- Log onto Docker registry `docker login`
- Test Docker `docker run -it --rm busybox`
- Deploy a Kubernetes pod 

## Basic commands

- Create deployment
- List pods
- Delete pods


## Advanced commands
- Port forward
- Inter pod communication
- Scale a pod
- Load balance a pod
